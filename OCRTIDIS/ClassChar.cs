﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OCRTIDIS
{
    class ClassChar
    {
        public string ReplaceAt(string cambiar, int index, char newChar)
        {
            if (cambiar == null)
            {
                throw new ArgumentNullException("cambiar");
            }
            char[] chars = cambiar.ToCharArray();
            chars[index] = newChar;
            return new string(chars);
        }
    }
}
