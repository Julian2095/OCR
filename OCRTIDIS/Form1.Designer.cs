﻿namespace OCRTIDIS
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.btnAceptar = new System.Windows.Forms.Button();
            this.c = new System.Windows.Forms.RichTextBox();
            this.imagen = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.axMiDocView1 = new AxMODI.AxMiDocView();
            this.button1 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.button2 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.imagen)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.axMiDocView1)).BeginInit();
            this.SuspendLayout();
            // 
            // btnAceptar
            // 
            this.btnAceptar.Location = new System.Drawing.Point(338, 112);
            this.btnAceptar.Name = "btnAceptar";
            this.btnAceptar.Size = new System.Drawing.Size(301, 47);
            this.btnAceptar.TabIndex = 0;
            this.btnAceptar.Text = "BUSCAR DERECHOS PATRIMONIALES";
            this.btnAceptar.UseVisualStyleBackColor = true;
            this.btnAceptar.Click += new System.EventHandler(this.button1_Click);
            // 
            // c
            // 
            this.c.Location = new System.Drawing.Point(18, 36);
            this.c.Name = "c";
            this.c.Size = new System.Drawing.Size(314, 433);
            this.c.TabIndex = 1;
            this.c.Text = "";
            this.c.TextChanged += new System.EventHandler(this.c_TextChanged);
            // 
            // imagen
            // 
            this.imagen.Location = new System.Drawing.Point(338, 36);
            this.imagen.Name = "imagen";
            this.imagen.Size = new System.Drawing.Size(301, 56);
            this.imagen.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.imagen.TabIndex = 2;
            this.imagen.TabStop = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(113, 20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(122, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "TEXTO ENCONTRADO";
            // 
            // axMiDocView1
            // 
            this.axMiDocView1.Enabled = true;
            this.axMiDocView1.Location = new System.Drawing.Point(984, 473);
            this.axMiDocView1.Name = "axMiDocView1";
            this.axMiDocView1.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("axMiDocView1.OcxState")));
            this.axMiDocView1.Size = new System.Drawing.Size(192, 192);
            this.axMiDocView1.TabIndex = 5;
            this.axMiDocView1.Visible = false;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(338, 324);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(301, 47);
            this.button1.TabIndex = 6;
            this.button1.Text = "SALIR";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(338, 271);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(301, 47);
            this.button3.TabIndex = 8;
            this.button3.Text = "LIMPIAR TEXTO";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(433, 20);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(124, 13);
            this.label2.TabIndex = 9;
            this.label2.Text = "IMAGEN A PROCESAR ";
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(338, 165);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(301, 47);
            this.button2.TabIndex = 10;
            this.button2.Text = "BUSCAR VALORES EN DEPOSITO";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click_3);
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(338, 218);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(301, 47);
            this.button4.TabIndex = 11;
            this.button4.Text = "BUSCAR CONSTANCIAS DE DEPOSITO";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(651, 498);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.axMiDocView1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.imagen);
            this.Controls.Add(this.c);
            this.Controls.Add(this.btnAceptar);
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "TIDIS";
            ((System.ComponentModel.ISupportInitialize)(this.imagen)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.axMiDocView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnAceptar;
        private System.Windows.Forms.RichTextBox c;
        private System.Windows.Forms.PictureBox imagen;
        private System.Windows.Forms.Label label1;
        private AxMODI.AxMiDocView axMiDocView1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button4;
    }
}

