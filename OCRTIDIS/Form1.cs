﻿using System;
using System.Windows.Forms;
using MODI;
using System.Drawing;
using System.IO;

namespace OCRTIDIS
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        string findChar;

        private void button1_Click(object sender, EventArgs e)
        {
            c.Text = "";
            OpenFileDialog open = new OpenFileDialog();
            open.Title = "Abrir archivo";
            open.Multiselect = true;
            open.Filter = "Jpg files (*.jpg)|*.jpg|PNG files (*.png)|*.png|TIF files (*.tif)|*.tif";

            if (open.ShowDialog() == DialogResult.OK)
            {
                int i = 1;
                foreach (String file in open.FileNames)
                {
                    String name = file;
                    Bitmap Picture = new Bitmap(name);
                    imagen.Image = (System.Drawing.Image)Picture;
                    string NombreIma = open.SafeFileName;

                    var miDocdument = new MODI.Document();
                    miDocdument.Create(@name);
                    miDocdument.OCR(MODI.MiLANGUAGES.miLANG_SPANISH, true, true);
                    MODI.Image image2 = (MODI.Image)miDocdument.Images[0];
                    Layout layout = image2.Layout;

                    string nombre_imagen = "";
                    //Ciclo que imprime cada letra del texto
                    foreach (Word word in layout.Words)
                    {
                        findChar = word.Text;

                        if (findChar.Contains("Q"))
                        {
                            findChar = findChar.Replace("Q", "0");
                        }
                        else if (findChar.Contains("O"))
                        {
                            findChar = findChar.Replace("O", "0");
                        }
                        else if (findChar.Contains("Y"))
                        {
                            findChar = findChar.Replace("Y", "9");
                        }
                        else if (findChar.Contains("l"))
                        {
                            findChar = findChar.Replace("l", "1");
                        }
                        else if (findChar.Contains("Ó"))
                        {
                            findChar = findChar.Replace("Ó", "o");
                        }
                        else if (findChar.Contains("G"))
                        {
                            findChar = findChar.Replace("G", "6");
                        }
                        nombre_imagen = nombre_imagen + findChar;
                        c.Text = c.Text + findChar;
                    }
                    //RutaA es la ruta donde tenemos los archivos a cambiar 
                    //string RutaA = file;
                    string RutaA = @"C:\Users\ACER\Desktop\TIDIS\VALORES\TIDIS" + i.ToString() + ".jpg";
                    try
                    {
                        try
                        {
                            //Ruta donde se creará la copia.
                            File.Copy(RutaA, @"C:\Users\ACER\Desktop\TIDIS\Renombradas\" + nombre_imagen + "-DEP" + ".jpg");
                        }
                        catch (Exception)
                        {
                            //Se captura la exception y se renombra con un (2).
                            File.Copy(RutaA, @"C:\Users\ACER\Desktop\TIDIS\Renombradas\" + nombre_imagen + "(2)" + "-DEP" + ".png");
                        }
                    }
                    catch (Exception)
                    {
                        try
                        {
                            //Ruta donde se creará la copia.
                            File.Copy(RutaA, @"C:\Users\ACER\Desktop\TIDIS\Renombradas\" + nombre_imagen + "-DEP" + ".jpg");
                        }
                        catch (Exception)
                        {
                            //Se captura la exception y se renombra con un (2).
                            File.Copy(RutaA, @"C:\Users\ACER\Desktop\TIDIS\Renombradas\" + nombre_imagen + "-" + "DEP" + ".png");
                        }
                    }

                    c.Text = c.Text + "\n ";
                    i++;
                }
                i = 0;
                MessageBox.Show("Conversión finalizada");
            }
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
        }

        private void button3_Click(object sender, EventArgs e)
        {
            c.Text = string.Empty;
            MessageBox.Show("Texto limpiado");
        }

        private void button2_Click_1(object sender, EventArgs e)
        {
        }

        private void c_TextChanged(object sender, EventArgs e)
        {
        }

        private void button2_Click_2(object sender, EventArgs e)
        {
        }

        private void button2_Click_3(object sender, EventArgs e)
        {
            c.Text = "";
            OpenFileDialog open = new OpenFileDialog();
            open.Title = "Abrir archivo";
            open.Multiselect = true;
            open.Filter = "Jpg files (*.jpg)|*.jpg|PNG files (*.png)|*.png|TIF files (*.tif)|*.tif";

            if (open.ShowDialog() == DialogResult.OK)
            {
                int i = 1;
                foreach (String file in open.FileNames)
                {
                    String name = file;
                    Bitmap Picture = new Bitmap(name);
                    imagen.Image = (System.Drawing.Image)Picture;
                    string NombreIma = open.SafeFileName;

                    var miDocdument = new MODI.Document();
                    miDocdument.Create(@name);
                    miDocdument.OCR(MODI.MiLANGUAGES.miLANG_SPANISH, true, true);
                    MODI.Image image2 = (MODI.Image)miDocdument.Images[0];
                    Layout layout = image2.Layout;

                    string nombre_imagen = "";
                    //Ciclo que imprime cada letra del texto
                    foreach (Word word in layout.Words)
                    {
                        findChar = word.Text;

                        if (findChar.Contains("Q"))
                        {
                            findChar = findChar.Replace("Q", "0");
                        }
                        else if (findChar.Contains("O"))
                        {
                            findChar = findChar.Replace("O", "0");
                        }
                        else if (findChar.Contains("Y"))
                        {
                            findChar = findChar.Replace("Y", "9");
                        }
                        else if (findChar.Contains("l"))
                        {
                            findChar = findChar.Replace("l", "1");
                        }
                        else if (findChar.Contains("Ó"))
                        {
                            findChar = findChar.Replace("Ó", "o");
                        }
                        else if (findChar.Contains("G"))
                        {
                            findChar = findChar.Replace("G", "6");
                        }
                        nombre_imagen = nombre_imagen + findChar;
                        c.Text = c.Text + findChar;
                    }
                    //RutaA es la ruta donde tenemos los archivos a cambiar  
                    string RutaA = @"C:\Users\ACER\Desktop\TIDIS\VALORES\TIDIS" + i.ToString() + ".jpg";
                    try
                    {
                        try
                        {
                            //Ruta donde se creará la copia.
                            File.Copy(RutaA, @"C:\Users\ACER\Desktop\TIDIS\Renombradas\" + nombre_imagen + "-VED" + ".jpg");
                        }
                        catch (Exception)
                        {
                            //Se captura la exception y se renombra con un (2).
                            File.Copy(RutaA, @"C:\Users\ACER\Desktop\TIDIS\Renombradas\" + nombre_imagen + "(2)" + "-VED" + ".png");
                        }
                    }
                    catch (Exception)
                    {
                        try
                        {
                            //Ruta donde se creará la copia.
                            File.Copy(RutaA, @"C:\Users\ACER\Desktop\TIDIS\Renombradas\" + nombre_imagen + "-VED" + ".jpg");
                        }
                        catch (Exception)
                        {
                            //Se captura la exception y se renombra con un (2).
                             File.Copy(RutaA, @"C:\Users\ACER\Desktop\TIDIS\Renombradas\" + nombre_imagen + "-" + "VED" + ".png");
                        }
                    }

                    c.Text = c.Text + "\n ";
                    i++;
                }
                i = 0;
                MessageBox.Show("Conversión finalizada");
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            c.Text = "";
            OpenFileDialog open = new OpenFileDialog();
            open.Title = "Abrir archivo";
            open.Multiselect = true;
            open.Filter = "Jpg files (*.jpg)|*.jpg|PNG files (*.png)|*.png|TIF files (*.tif)|*.tif";

            if (open.ShowDialog() == DialogResult.OK)
            {
                int i = 1;
                foreach (String file in open.FileNames)
                {
                    String name = file;
                    Bitmap Picture = new Bitmap(name);
                    imagen.Image = (System.Drawing.Image)Picture;
                    string NombreIma = open.SafeFileName;

                    var miDocdument = new MODI.Document();
                    miDocdument.Create(@name);
                    miDocdument.OCR(MODI.MiLANGUAGES.miLANG_SPANISH, true, true);
                    MODI.Image image2 = (MODI.Image)miDocdument.Images[0];
                    Layout layout = image2.Layout;

                    string nombre_imagen = "";
                    //Ciclo que imprime cada letra del texto
                    foreach (Word word in layout.Words)
                    {
                        findChar = word.Text;

                        if (findChar.Contains("Q"))
                        {
                            findChar = findChar.Replace("Q", "0");
                        }
                        else if (findChar.Contains("O"))
                        {
                            findChar = findChar.Replace("O", "0");
                        }
                        else if (findChar.Contains("Y"))
                        {
                            findChar = findChar.Replace("Y", "9");
                        }
                        else if (findChar.Contains("l"))
                        {
                            findChar = findChar.Replace("l", "1");
                        }
                        else if (findChar.Contains("Ó"))
                        {
                            findChar = findChar.Replace("Ó", "o");
                        }
                        else if (findChar.Contains("G"))
                        {
                            findChar = findChar.Replace("G", "6");
                        }

                        nombre_imagen = nombre_imagen + findChar;
                        c.Text = c.Text + findChar;
                    }
                    //RutaA es la ruta donde tenemos los archivos a cambiar 
                    //string RutaA = file;

                    string RutaA = @"C:\Users\ACER\Desktop\TIDIS\VALORES\TIDIS" + i.ToString() + ".jpg";
                    try
                    {
                        //Ruta donde se creará la copia.
                        File.Copy(RutaA, @"C:\Users\ACER\Desktop\TIDIS\Renombradas\" + nombre_imagen + ".jpg");
                    }
                    catch (Exception)
                    {
                        //Se captura la exception y se renombra con un (2).
                        File.Copy(RutaA, @"C:\Users\ACER\Desktop\TIDIS\Renombradas\" + nombre_imagen + "(2)" + ".jpg");
                    }
                    c.Text = c.Text + "\n ";
                    i++;
                }
                i = 0;
                MessageBox.Show("Conversión finalizada");
            }
        }
    }
}

